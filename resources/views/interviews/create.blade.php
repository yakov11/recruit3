@extends('layouts.app')

@section('title', 'Create interview')

@section('content')
        <h1>Create candidate</h1>
        <form method = "post" action = "{{action('InterviewController@store')}}">
        @csrf 
        <div class="form-group">
            <label for = "name">interview date</label>
            <input type = "text" class="form-control" name = "date">
        </div>     
        <div class="form-group">
            <label for = "email">interview text</label>
            <input type = "text" class="form-control" name = "text">
        </div> 
        
        <div class="col-md-6">
        <label for = "candidate_id">choose candidate</label>
                        <select class="form-control" name="candidate_id">                                                                         
                          @foreach ($candidates as $candidate)
                          <option value="{{$candidate->id }}"> 
                              {{ $candidate->name }} 
                          </option>
                          @endforeach    
                        </select>
                        <div>
                        <div class="col-md-6">
        <label for = "user_id">choose user</label>
                        <select class="form-control" name="user_id" >                                                                         
                          @foreach ($users as $user)
                          <option value="{{$user->id }}"> 
                              {{ $user->name }} 
                          </option>
                          @endforeach    
                        </select>
                        <div>
            <input type = "submit" name = "submit" value = "save interview">
        </div>            
                   
            
                              
        </form>    
@endsection

