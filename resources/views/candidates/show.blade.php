@extends('layouts.app')

@section('title', 'interviews')

@section('content')


<h1>List of interviews</h1>
<table class = "table table-dark">
    <tr>
        <th>id</th><th>date</th><th>text</th><th>candidate</th><th>owner</th>
    </tr>
    <!-- the table data -->
    <div><a href =  "{{url('/interviews/create')}}"> Add new interview</a></div>
    @foreach($interviews as $interview)
        <tr>
            <td>{{$interview->id}}</td>
            <td>{{$interview->date}}</td>
            <td>{{$interview->text}}</td>
            
        </tr>
        @endforeach
        </div>  
</table>
@endsection

