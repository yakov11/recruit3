<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Candidate;
use App\Interview;
use App\User;
use App\Status;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;

class InterviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Gate::authorize('check-admin');
        Gate::authorize('check-manager');
        $interviews = Interview::all();
        $candidates = Candidate::all();
        $users = User::all();
        return view('interviews.create', compact('interviews','candidates','users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $interview = new Interview();
        $int = $interview->create($request->all());
        $interview->candidate_id = $request->candidate_id; 
        $interview->user_id = $request->user_id; 
        $int->save();
        $interviews = Interview::all();
        $candidates = Candidate::all();
        $users = User::all();
        return view('interviews.show', compact('interviews','candidates','users'));
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        
        $interviews = Interview::all();
            
        return view('interviews.show', compact('interviews'));
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function changeCandidate($id, $cid = null){
      
        
        $interview = Interview::findOrFail($id);
        $interview->candidate_id = $cid;
        $interview->save(); 
        return back();
    }
    public function changeUser($id, $cid = null){
      
        $interview = Interview::findOrFail($id);
        $interview->user_id = $cid;
        $interview->save(); 
        return back();
    }
    public function myinterviews()
    {        
        $userId = Auth::id();
        $user = User::findOrFail($userId);
        $interviews = $user->interviews;
        if(isset ($interviews)){  $candidates = Candidate::all();
            $users = User::all();    
            return view('interviews.show', compact('interviews','candidates','users')); }
        else{
            return view('interviews.good');
           
        }   
        
    }

    
}
