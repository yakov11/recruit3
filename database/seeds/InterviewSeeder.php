<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class InterviewSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('interviews')->insert([
            [
                'date' => '2020-07-16',
                'text' => 'well',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'date' => '2020-08-16',
                'text' => 'bad',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],            
            [
                'date' => '2020-09-16',
                'text' => 'nice',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]
            ]);         
    }
}
